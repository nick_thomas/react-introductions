import React, {useEffect, useState} from "react";
import {GridColumn, GridRow, Grid, Input, Button} from "semantic-ui-react";
import {getNews } from "../services/api";


// 1 -> 
// 2 ->
//

const NewsComponent = ({news, updateLocalState }) =><div>
	{news.length > 0 && news.map((item)=> <p key={item.url}>{item.title}<Button onClick={()=> updateLocalState(item.title) }>Save</Button></p>) }
</div>


	const Main = () => {
		let [news, setNews] = useState([]);
		let [local, setLocal] = useState([]);

		const filterNews = (event) => {
			event.preventDefault();
			const searchedContent = news.filter((item)=> item.title.toLowerCase().includes(event.target.value));
			setNews([...searchedContent]);
		}

		const fetchingNews = async () => {
			const response = await getNews();
			const { articles} = response;
			setNews([...articles]);
		}

		const updateLocalState = (item) => {
			let itemStore;
			if(localStorage.getItem('storedNews')){
				let currentItem = JSON.parse(localStorage.getItem('storedNews'));
				itemStore = [...currentItem,item];
			} else {
				itemStore = [item];
			}
			setLocal([...itemStore]);
			localStorage.setItem('storedNews',JSON.stringify(itemStore));
		}

		useEffect(()=>{

			fetchingNews();
		},[])
		return <Grid divided='vertically'>
			<GridRow columns={3}>
				<GridColumn>
					<Input focus placeholder="Search" onChange={(e)=>filterNews(e) } />
					<Button onClick={()=> fetchingNews() }> Reset</Button>
				</GridColumn>
				<GridColumn>
					<NewsComponent news={news} updateLocalState={updateLocalState} />
				</GridColumn>
				<GridColumn>
					<div>
						{local.length === 0 ? <p>Local Storage is empty </p> : <ul>{local.map((item)=><li key={item}>{item}</li>) }</ul> }
					</div>
				</GridColumn>
			</GridRow>
		</Grid>
	}

export default Main
