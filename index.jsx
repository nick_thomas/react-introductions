import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css'
import Main from './components/main';

const App = () => {
	return <div><Main /></div>
}

ReactDOM.render(<App />,document.getElementById('root'))
